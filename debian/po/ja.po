#
#    Translators, if you are not familiar with the PO format, gettext
#    documentation is worth reading, especially sections dedicated to
#    this format, e.g. by running:
#         info -n '(gettext)PO Files'
#         info -n '(gettext)Header Entry'
#
#    Some information specific to po-debconf are available at
#            /usr/share/doc/po-debconf/README-trans
#         or http://www.debian.org/intl/l10n/po-debconf/README-trans
#
#    Developers do not need to manually edit POT or PO files.
#
msgid ""
msgstr ""
"Project-Id-Version: exim4 4.67\n"
"Report-Msgid-Bugs-To: pkg-exim4-maintainers@lists.alioth.debian.org\n"
"POT-Creation-Date: 2007-07-18 08:29+0200\n"
"PO-Revision-Date: 2007-07-14 16:52+0900\n"
"Last-Translator: Kenshi Muto <kmuto@debian.org>\n"
"Language-Team: Japanese <debian-japanese@lists.debian.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: boolean
#. Description
#: ../exim4-base.templates:1001
msgid "Remove undelivered messages in spool directory?"
msgstr "スプールディレクトリにある未配送のメッセージを削除しますか?"

#. Type: boolean
#. Description
#: ../exim4-base.templates:1001
msgid ""
"There are e-mail messages in the Exim spool directory /var/spool/exim4/"
"input/ which have not yet been delivered. Removing Exim will cause them to "
"remain undelivered until Exim is re-installed."
msgstr ""
"exim スプールディレクトリ /var/spool/exim4/input にまだ配送されていないメール"
"メッセージがあります。Exim を削除すると、Exim が再インストールされるまでこれ"
"らは未配送のままとなります。"

#. Type: boolean
#. Description
#: ../exim4-base.templates:1001
msgid ""
"If this option is not chosen, the spool directory is kept, allowing the "
"messages in the queue to be delivered at a later date after Exim is re-"
"installed."
msgstr ""
"この選択肢に「はい」と答えない場合、後日 Exim を再インストールしたあと、"
"キューにあるメッセージを許可するよう、スプールディレクトリは保持されます。"

#. Type: error
#. Description
#: ../exim4-base.templates:2001 ../exim4-daemon-heavy.templates:1001
#: ../exim4-daemon-light.templates:1001 ../exim4.templates:1001
msgid "Reconfigure exim4-config instead of this package"
msgstr "このパッケージの代わりに exim4-config を再設定してください"

#. Type: error
#. Description
#: ../exim4-base.templates:2001 ../exim4-daemon-heavy.templates:1001
#: ../exim4-daemon-light.templates:1001 ../exim4.templates:1001
msgid ""
"Exim4 has its configuration factored out into a dedicated package, exim4-"
"config. To reconfigure Exim4, use 'dpkg-reconfigure exim4-config'."
msgstr ""
"Exim4 は、自身の設定を専用パッケージ exim4-config に委ねています。Exim4 を再"
"設定したい場合には、'dpkg-reconfigure exim4-config' を利用してください。"

#. Type: select
#. Choices
#. Translators beware! the following six strings form a single
#. Choices menu. - Every one of these strings has to fit in a standard
#. 80 characters console, as the fancy screen setup takes up some space
#. try to keep below ~71 characters.
#. DO NOT USE commas (,) in Choices translations otherwise
#. this will break the choices shown to users
#: ../exim4-config.templates:1001
msgid "internet site; mail is sent and received directly using SMTP"
msgstr "インターネットサイト; メールは SMTP を使って直接送受信される"

#. Type: select
#. Choices
#. Translators beware! the following six strings form a single
#. Choices menu. - Every one of these strings has to fit in a standard
#. 80 characters console, as the fancy screen setup takes up some space
#. try to keep below ~71 characters.
#. DO NOT USE commas (,) in Choices translations otherwise
#. this will break the choices shown to users
#: ../exim4-config.templates:1001
msgid "mail sent by smarthost; received via SMTP or fetchmail"
msgstr "スマートホストでメール送信; SMTP または fetchmail で受信する"

#. Type: select
#. Choices
#. Translators beware! the following six strings form a single
#. Choices menu. - Every one of these strings has to fit in a standard
#. 80 characters console, as the fancy screen setup takes up some space
#. try to keep below ~71 characters.
#. DO NOT USE commas (,) in Choices translations otherwise
#. this will break the choices shown to users
#: ../exim4-config.templates:1001
msgid "mail sent by smarthost; no local mail"
msgstr "スマートホストでメール送信; ローカルメールなし"

#. Type: select
#. Choices
#. Translators beware! the following six strings form a single
#. Choices menu. - Every one of these strings has to fit in a standard
#. 80 characters console, as the fancy screen setup takes up some space
#. try to keep below ~71 characters.
#. DO NOT USE commas (,) in Choices translations otherwise
#. this will break the choices shown to users
#: ../exim4-config.templates:1001
msgid "local delivery only; not on a network"
msgstr "ローカル配信のみ; ネットワークなし"

#. Type: select
#. Choices
#. Translators beware! the following six strings form a single
#. Choices menu. - Every one of these strings has to fit in a standard
#. 80 characters console, as the fancy screen setup takes up some space
#. try to keep below ~71 characters.
#. DO NOT USE commas (,) in Choices translations otherwise
#. this will break the choices shown to users
#: ../exim4-config.templates:1001
msgid "no configuration at this time"
msgstr "今は設定しない"

#. Type: select
#. Description
#: ../exim4-config.templates:1002
msgid "General type of mail configuration:"
msgstr "メール設定の一般的なタイプ:"

#. Type: select
#. Description
#: ../exim4-config.templates:1002
msgid ""
"Please select the mail server configuration type that best meets your needs."
msgstr "必要とするものに最適と思われるメールサーバ設定形式を選択してください。"

#. Type: select
#. Description
#: ../exim4-config.templates:1002
msgid ""
"Systems with dynamic IP addresses, including dialup systems, should "
"generally be configured to send outgoing mail to another machine, called a "
"'smarthost' for delivery because many receiving systems on the Internet "
"block incoming mail from dynamic IP addresses as spam protection."
msgstr ""
"ダイヤルアップシステムなどの動的 IP アドレスのシステムでは、一般に送出する"
"メールを配信のために別のマシン (「スマートホスト」と呼ばれる) に送るよう設定"
"すべきでしょう。インターネット上の受信システムの多くは、迷惑メール対策として"
"動的 IP アドレスからやってくるメールをブロックしているからです。"

#. Type: select
#. Description
#: ../exim4-config.templates:1002
msgid ""
"A system with a dynamic IP address can receive its own mail, or local "
"delivery can be disabled entirely (except mail for root and postmaster)."
msgstr ""
"動的 IP アドレスのシステムは、自身のメールを受け取るようにするか、ローカル配"
"送を (root と postmaster へのメールを除き) すべて無効にするかできます。"

#. Type: boolean
#. Description
#: ../exim4-config.templates:2001
msgid "Really leave the mail system unconfigured?"
msgstr "本当にメールシステムを未設定のままにしますか?"

#. Type: boolean
#. Description
#: ../exim4-config.templates:2001
msgid ""
"Until the mail system is configured, it will be broken and cannot be used. "
"Configuration at a later time can be done either by hand or by running 'dpkg-"
"reconfigure exim4-config' as root."
msgstr ""
"メールシステムが設定されるまで、メールシステムは壊れた状態で利用できないもの"
"となります。あとで手動で、または root として 'dpkg-reconfigure exim4-config' "
"を実行することで設定できます。"

#. Type: string
#. Description
#: ../exim4-config.templates:3001
msgid "System mail name:"
msgstr "システムメール名:"

#. Type: string
#. Description
#: ../exim4-config.templates:3001
msgid ""
"The 'mail name' is the domain name used to 'qualify' mail addresses without "
"a domain name."
msgstr ""
"「メール名」は、ドメイン名がないときにメールアドレスを「修飾」するために使わ"
"れるドメイン名です。"

#. Type: string
#. Description
#: ../exim4-config.templates:3001
msgid ""
"This name will also be used by other programs. It should be the single, "
"fully qualified domain name (FQDN)."
msgstr ""
"この名前はほかのプログラムによっても使われます。これは、単一の完全修飾ドメイ"
"ン名 (FQDN) にすべきです。"

#. Type: string
#. Description
#: ../exim4-config.templates:3001
msgid ""
"Thus, if a mail address on the local host is foo@example.org, the correct "
"value for this option would be example.org."
msgstr ""
"たとえば、ローカルホストのメールアドレスが foo@example.org の場合、ここで入力"
"する正しい値は example.org となるでしょう。"

#. Type: string
#. Description
#: ../exim4-config.templates:3001
msgid ""
"This name won't appear on From: lines of outgoing messages if rewriting is "
"enabled."
msgstr ""
"書き換えを有効にすると、この名前は送出するメッセージの From: 行に出現しませ"
"ん。"

#. Type: string
#. Description
#: ../exim4-config.templates:4001
msgid "Other destinations for which mail is accepted:"
msgstr "メールを受け取るその他の宛先:"

#. Type: string
#. Description
#: ../exim4-config.templates:4001
msgid ""
"Please enter a semicolon-separated list of recipient domains for which this "
"machine should consider itself the final destination. These domains are "
"commonly called 'local domains'. The local hostname (${fqdn}) and "
"'localhost' are always added to the list given here."
msgstr ""
"このマシン自身が最終的な宛先と見なされるべきドメインのリストをセミコロンで区"
"切って指定してください。これらのドメインは一般に「ローカルドメイン」と呼ばれ"
"ます。ローカルホスト名 (${fqdn}) および 'localhost' は常にこのリストに追加さ"
"れます。"

#. Type: string
#. Description
#: ../exim4-config.templates:4001
msgid ""
"By default all local domains will be treated identically. If both a.example "
"and b.example are local domains, acc@a.example and acc@b.example will be "
"delivered to the same final destination. If different domain names should be "
"treated differently, it is necessary to edit the config files afterwards."
msgstr ""
"デフォルトでは、すべてのドメインは同一のものとして扱われます。a.example と b."
"example の両方がローカルドメインである場合、acc@a.example と acc@b.example は"
"同じ最終宛先に配送されます。異なるドメイン名を別々のものとして扱いたいときに"
"は、設定ファイルをあとで編集する必要があります。"

#. Type: string
#. Description
#: ../exim4-config.templates:5001
msgid "Domains to relay mail for:"
msgstr "メールをリレーするドメイン:"

#. Type: string
#. Description
#: ../exim4-config.templates:5001
msgid ""
"Please enter a semicolon-separated list of recipient domains for which this "
"system will relay mail, for example as a fallback MX or mail gateway. This "
"means that this system will accept mail for these domains from anywhere on "
"the Internet and deliver them according to local delivery rules."
msgstr ""
"たとえばフォールバック MX またはメールゲートウェイとしてこのシステムがメール"
"をリレーする、受信ドメインのリストをセミコロンで区切って指定してください。つ"
"まり、このシステムがこれらのドメインからのメールをインターネット上のどこから"
"も受け付け、ローカル配送ルールに従って配送することを意味します。"

#. Type: string
#. Description
#: ../exim4-config.templates:5001
msgid "Do not mention local domains here. Wildcards may be used."
msgstr ""
"ここではローカルドメインについては記述しないでください。ワイルドカードも利用"
"できます。"

#. Type: string
#. Description
#: ../exim4-config.templates:6001
msgid "Machines to relay mail for:"
msgstr "メールをリレーするマシン:"

#. Type: string
#. Description
#: ../exim4-config.templates:6001
msgid ""
"Please enter a semicolon-separated list of IP address ranges for which this "
"system will unconditionally relay mail, functioning as a smarthost."
msgstr ""
"このシステムがスマートホストの役割としてメールを無条件にリレー可能な IP アド"
"レス範囲のリストを、セミコロンで区切って指定してください。"

#. Type: string
#. Description
#: ../exim4-config.templates:6001
msgid ""
"You should use the standard address/prefix format (e.g. 194.222.242.0/24 or "
"5f03:1200:836f::/48)."
msgstr ""
"標準的なアドレス/長さのフォーマットを利用すべきです (たとえば "
"194.222.242.0/24 や 5f03:1200:836f::/48)。"

#. Type: string
#. Description
#: ../exim4-config.templates:6001
msgid ""
"If this system should not be a smarthost for any other host, leave this list "
"blank."
msgstr ""
"このシステムをほかのホスト向けのスマートホストにしようとするわけではないな"
"ら、このリストを空のままにしておいてください。"

#. Type: string
#. Description
#: ../exim4-config.templates:7001
msgid "Visible domain name for local users:"
msgstr "表示するローカルユーザのドメイン名:"

#. Type: string
#. Description
#: ../exim4-config.templates:7001
msgid ""
"The option to hide the local mail name in outgoing mail was enabled. It is "
"therefore necessary to specify the domain name this system should use for "
"the domain part of local users' sender addresses."
msgstr ""
"送出メール内のローカルメール名を隠すオプションが有効です。そのため、このシス"
"テムがローカルユーザの送信者アドレスのドメイン部分に使う、ドメイン名を指定す"
"る必要があります。"

#. Type: string
#. Description
#: ../exim4-config.templates:8001
msgid "IP address or host name of the outgoing smarthost:"
msgstr "送出スマートホストの IP アドレスまたはホスト名:"

#. Type: string
#. Description
#: ../exim4-config.templates:8001
msgid ""
"Please enter the IP address or the host name of a mail server that this "
"system should use as outgoing smarthost. If the smarthost only accepts your "
"mail on a port different from TCP/25, append two colons and the port number "
"(for example smarthost.example::587 or 192.168.254.254::2525). Colons in "
"IPv6 addresses need to be doubled."
msgstr ""
"このシステムが送出スマートホストとして使うメールサーバの IP アドレスまたはホ"
"スト名を指定してください。スマートホストが TCP/25 とは異なるポートからのみの"
"メールを受け付けるのであれば、2 つのコロンとポート番号を追加してください (あ"
"とえば smarthost.example::587 あるいは 192.168.254.254::2525)。IPv6 アドレス"
"でのコロンはさらに二重にする必要があります。"

#. Type: string
#. Description
#: ../exim4-config.templates:8001
msgid ""
"If the smarthost requires authentication, please refer to the Debian-"
"specific README files in /usr/share/doc/exim4-base for notes about setting "
"up SMTP authentication."
msgstr ""
"スマートホストが認証を必要とする場合、SMTP 認証のセットアップについて書かれ"
"た Debian 固有の README ファイルを参照してください。"

#. Type: string
#. Description
#: ../exim4-config.templates:9001
msgid "Root and postmaster mail recipient:"
msgstr "root と postmaster のメール受信者:"

#. Type: string
#. Description
#: ../exim4-config.templates:9001
msgid ""
"Mail for the 'postmaster', 'root', and other system accounts needs to be "
"redirected to the user account of the actual system administrator."
msgstr ""
"「postmaster」「root」およびその他のシステムアカウントへのへのメールは、実際"
"のシステム管理者のユーザアカウントに転送される必要があります。"

#. Type: string
#. Description
#: ../exim4-config.templates:9001
msgid ""
"If this value is left empty, such mail will be saved in /var/mail/mail, "
"which is not recommended."
msgstr ""
"この値を空のままにすると、そのようなメールは /var/mail/mail に保存されます"
"が、これはお勧めできません。"

#. Type: string
#. Description
#: ../exim4-config.templates:9001
msgid ""
"Note that postmaster's mail should be read on the system to which it is "
"directed, rather than being forwarded elsewhere, so (at least one of) the "
"users listed here should not redirect their mail off this machine. A 'real-' "
"prefix can be used to force local delivery."
msgstr ""
"postmaster のメールは別のところに転送するよりも直接そのシステムで読まれるべき"
"ものであることに注意してください。このため、ここで指定するユーザ (の少なくと"
"もそのうち 1 人) は、このマシンの外にリダイレクトするようなものではないように"
"すべきです。強制的にローカルに配送するには、'real-' プレフィクスを使います。"

#. Type: string
#. Description
#: ../exim4-config.templates:9001
msgid "Multiple user names need to be separated by spaces."
msgstr "複数のユーザ名はスペースで区切る必要があります。"

#. Type: string
#. Description
#: ../exim4-config.templates:10001
msgid "IP-addresses to listen on for incoming SMTP connections:"
msgstr "入力側 SMTP 接続をリスンする IP アドレス:"

#. Type: string
#. Description
#: ../exim4-config.templates:10001
msgid ""
"Please enter a semicolon-separated list of IP addresses. The Exim SMTP "
"listener daemon will listen on all IP addresses listed here."
msgstr ""
"IP アドレスのリストをセミコロンで区切って指定してください。Exim SMTP リスナ"
"デーモンは、ここで挙げられたすべての IP アドレスをリスンします。"

#. Type: string
#. Description
#: ../exim4-config.templates:10001
msgid ""
"An empty value will cause Exim to listen for connections on all available "
"network interfaces."
msgstr ""
"この値を空にすると、Exim はすべての利用可能なネットワークインターフェイスの接"
"続をリスンするようになります。"

#. Type: string
#. Description
#: ../exim4-config.templates:10001
msgid ""
"If this system only receives mail directly from local services (and not from "
"other hosts), it is suggested to prohibit external connections to the local "
"Exim daemon. Such services include e-mail programs (MUAs) which talk to "
"localhost only as well as fetchmail. External connections are impossible "
"when 127.0.0.1 is entered here, as this will disable listening on public "
"network interfaces."
msgstr ""
"このシステムが (ほかのホストからではなく) ローカルサービスから直接メールを受"
"け取るのであれば、ローカルの Exim デーモンへの外部からの接続を禁止するのもよ"
"いでしょう。このようなサービスには、fetchmail 同様にローカルホストとやり取り"
"する電子メールプログラム (MUA) も含まれます。ここで 127.0.0.1 と入力すると、"
"パブリックなネットワークインターフェイスのリスニングを無効にするので、外部か"
"ら接続できなくなります。"

#. Type: boolean
#. Description
#: ../exim4-config.templates:11001
msgid "Keep number of DNS-queries minimal (Dial-on-Demand)?"
msgstr "DNS クエリの数を最小限に留めますか (ダイヤルオンデマンド)?"

#. Type: boolean
#. Description
#: ../exim4-config.templates:11001
msgid ""
"In normal mode of operation Exim does DNS lookups at startup, and when "
"receiving or delivering messages. This is for logging purposes and allows "
"keeping down the number of hard-coded values in the configuration."
msgstr ""
"通常の動作モードでは、Exim は起動時、およびメッセージの受信時と配送時に DNS "
"ルックアップを行います。これは、記録用であり、設定ファイルにハードコードされ"
"た値の数に抑えることができます。"

#. Type: boolean
#. Description
#: ../exim4-config.templates:11001
msgid ""
"If this system does not have a DNS full service resolver available at all "
"times (for example if its Internet access is a dial-up line using dial-on-"
"demand), this might have unwanted consequences. For example, starting up "
"Exim or running the queue (even with no messages waiting) might trigger a "
"costly dial-up-event."
msgstr ""
"このシステムが完全な DNS サービスリゾルバを常時利用できない場合 (たとえばイン"
"ターネットアクセスがダイヤルアップオンデマンドを使ったダイヤルアップ回線の場"
"合)、これは望ましくない結果となり得ます。たとえばExim の起動時やキューの実行"
"時 (待機中のメッセージがないときでも) に、コストの高いダイヤルアップイベント"
"を引き起こす恐れがあります。"

#. Type: boolean
#. Description
#: ../exim4-config.templates:11001
msgid ""
"This option should be selected if this system is using Dial-on-Demand. If it "
"has always-on Internet access, this option should be disabled."
msgstr ""
"このシステムがダイヤルオンデマンドを使っている場合には、「はい」と答えるべき"
"です。インターネットアクセスに常時接続しているのであれば、「いいえ」とすべき"
"です。"

#. Type: title
#. Description
#: ../exim4-config.templates:12001
msgid "Mail Server configuration"
msgstr "メールサーバの設定:"

#. Type: boolean
#. Description
#: ../exim4-config.templates:13001
msgid "Split configuration into small files?"
msgstr "設定を小さなファイルに分割しますか?"

#. Type: boolean
#. Description
#: ../exim4-config.templates:13001
msgid ""
"The Debian exim4 packages can either use 'unsplit configuration', a single "
"monolithic file (/etc/exim4/exim4.conf.template) or 'split configuration', "
"where the actual Exim configuration files are built from about 50 smaller "
"files in /etc/exim4/conf.d/."
msgstr ""
"Debian の exim4 パッケージは、1 つのモノリシックなファイルである「単一設定」"
"(/etc/exim4/exim4.conf.template) か /etc/exim4/conf.d/ に置かれる約 50 の小さ"
"なファイルから実際のファイルが構成される「分割設定」のどちらでも利用できま"
"す。"

#. Type: boolean
#. Description
#: ../exim4-config.templates:13001
msgid ""
"Unsplit configuration is better suited for large modifications and is "
"generally more stable, whereas split configuration offers a comfortable way "
"to make smaller modifications but is more fragile and might break if "
"modified carelessly."
msgstr ""
"単一設定は大きな変更をするのに向いており、一般的により安定しているのに対し、"
"分割設定は小さな変更を行うのに楽な方法を提供します (ただし脆く、配慮せずに変"
"更すると壊れるかもしれません)。"

#. Type: boolean
#. Description
#: ../exim4-config.templates:13001
msgid ""
"A more detailed discussion of split and unsplit configuration can be found "
"in the Debian-specific README files in /usr/share/doc/exim4-base."
msgstr ""
"分割設定と単一設定の詳細な議論については、/usr/share/doc/exim4-base にある "
"Debian 固有の README ファイルで参照できます。"

#. Type: boolean
#. Description
#: ../exim4-config.templates:14001
msgid "Hide local mail name in outgoing mail?"
msgstr "送出するメールでローカルメール名を隠しますか?"

#. Type: boolean
#. Description
#: ../exim4-config.templates:14001
msgid ""
"The headers of outgoing mail can be rewritten to make it appear to have been "
"generated on a different system. If this option is chosen, '${mailname}', "
"'localhost' and '${dc_other_hostnames}' in From, Reply-To, Sender and Return-"
"Path are rewritten."
msgstr ""
"異なるシステムで生成されたかのように見えるよう、送出するメールのヘッダは書き"
"換えできます。ここで「はい」と答えると、From、Reply-To、Sender、Return-Path "
"の '${mailname}'、'localhost'、'${dc_other_hostnames}' が置き換えられます。"

#. Type: select
#. Choices
#: ../exim4-config.templates:15001
msgid "mbox format in /var/mail/"
msgstr "/var/mail/ 内の mbox 形式"

#. Type: select
#. Choices
#: ../exim4-config.templates:15001
msgid "Maildir format in home directory"
msgstr "ホームディレクトリ内の Maildir 形式"

#. Type: select
#. Description
#: ../exim4-config.templates:15002
msgid "Delivery method for local mail:"
msgstr "ローカルメールの配送方式:"

#. Type: select
#. Description
#: ../exim4-config.templates:15002
msgid ""
"Exim is able to store locally delivered email in different formats. The most "
"commonly used ones are mbox and Maildir. mbox uses a single file for the "
"complete mail folder stored in /var/mail/. With Maildir format every single "
"message is stored in a separate file in ~/Maildir/."
msgstr ""
"Exim はローカルに配送された電子メールを異なる形式で格納できます。ほとんどの場"
"合は、mbox と Maildir のどちらかが使われます。mbox はすべてのメールフォルダ"
"を /var/mail に単一ファイルで格納します。Maildir 形式では、各個のメッセージ"
"は ~/Maildir/ に分割されたファイルとして格納されます。"

#. Type: select
#. Description
#: ../exim4-config.templates:15002
msgid ""
"Please note that most mail tools in Debian expect the local delivery method "
"to be mbox in their default."
msgstr ""
"ほとんどのメールツールは、ローカル配送方式が mbox であるとデフォルトでは仮定"
"していることに注意してください。"
